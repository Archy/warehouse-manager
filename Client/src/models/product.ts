export class Product {
    name: string;
    manufacturer: string;
    price: number;
    quantity: number;
    _links: any;

	constructor(name: string, manufacturer: string, price: number, quantity: number) {
		this.name = name;
		this.manufacturer = manufacturer;
		this.price = price;
		this.quantity = quantity;
    }
}

export interface Product {
    name: string;
    manufacturer: string;
    price: number;
    quantity: number;
}