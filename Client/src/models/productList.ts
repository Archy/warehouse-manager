import { Product } from './product';

export class ProductList {
    _embedded: EmbeddedList;
    _links: any;
    page: any;

	constructor(_embedded: EmbeddedList, _links: any, page: any) {
		this._embedded = _embedded;
		this._links = _links;
		this.page = page;
    }
}

export class EmbeddedList {
    productList: Product[];
}