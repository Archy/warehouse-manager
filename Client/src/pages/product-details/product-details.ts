import { Product } from './../../models/product';
import { ProductsProvider } from './../../providers/products/products';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { FormControl, FormGroup, Validators } from '@angular/forms';

/**
 * Generated class for the ProductDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product-details',
  templateUrl: 'product-details.html',
})
export class ProductDetailsPage implements OnInit {
  name: string;
  product: Product;
  lastErrorMsg: string;
  links: any;

  updateForm: FormGroup;
  isReadyToUpdate: boolean;


  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public productsProvider: ProductsProvider, public loadingCtrl: LoadingController) {
    this.name = navParams.get('name');
    this.links = navParams.get('links');
    console.log('Products details: ' + this.name);

    this.updateForm =  new FormGroup({
      delta: new FormControl(0, Validators.required),
      sign: new FormControl(),
    });
    this.isReadyToUpdate = false;

    this.updateForm.valueChanges.subscribe((v) => {
      const delta  = this.updateForm.value.delta * (this.updateForm.value.sign ? -1 : 1);
      this.isReadyToUpdate = this.updateForm.valid && this.validateDelta(delta);
    });
  }

  ngOnInit(): void {
    this.productsProvider.get(this.name, this.links.self.href).subscribe(
      p => {
        console.log('Product details loaded');
        console.log(p);
        this.product = p;
      },
      err => {
        this.lastErrorMsg = err.error;
      },
    );
  }

  updateQuantity() {
    if (!this.updateForm.valid) { return; }
    const delta  = this.updateForm.value.delta * (this.updateForm.value.sign ? -1 : 1);
    console.log('Updating product quantity by ' +  delta);


    const loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    this.productsProvider.updateQuantity(this.name, delta, this.links.updateQuantity.href).subscribe(
      p => {
        console.log('Product quantity updated');
        console.log(p);
        this.product = p;
        this.isReadyToUpdate = this.validateDelta(delta);
        loader.dismiss();
      },
      err => {
        this.lastErrorMsg = err.error;
        loader.dismiss();
      },
    );
  }

  doRefresh(refresher) {
    this.productsProvider.get(this.name, this.links.self.href).subscribe(
      p => {
        console.log('Product details loaded');
        console.log(p);
        this.product = p;
        refresher.complete();
      },
      err => {
        this.lastErrorMsg = err.error;
        refresher.complete();
      },
    );
  }

  private validateDelta(delta: number): boolean {
    if (delta == 0) {
      return false;
    }
    if (delta > 0) {
      return true;
    }
    return this.product.quantity >= Math.abs(delta);
  }
}
