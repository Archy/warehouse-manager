import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, ViewController, LoadingController, NavParams, } from 'ionic-angular';
import { ProductsProvider } from './../../providers/products/products';

/**
 * Generated class for the ProductAddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-product-add',
  templateUrl: 'product-add.html',
})
export class ProductAddPage {
  createForm: FormGroup;
  isReadyToSave: boolean;
  lastErrorMsg: string;
  addLink: string;


  constructor(public navCtrl: NavController, public viewCtrl: ViewController, public navParams: NavParams,
    public loadingCtrl: LoadingController, public productsProvider: ProductsProvider) {
    this.addLink = navParams.get('addLink');
    this.createForm =  new FormGroup({
      name: new FormControl('', Validators.required),
      manufacturer: new FormControl('', Validators.required),
      price: new FormControl('', [Validators.required]),
    });
    this.isReadyToSave = false;

    this.createForm.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.createForm.valid;
    });
  }

  cancel() {
    console.info('Canceling add');
    this.viewCtrl.dismiss();
  }

  createProduct() {
    if (!this.createForm.valid) { return; }
    console.log('Adding new products: ');
    console.log(this.createForm.value);

    const loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();

    this.productsProvider.addNew(this.createForm.value.name, this.createForm.value.manufacturer, this.createForm.value.price, this.addLink)
      .subscribe(
        product => {
          console.log('Products added: ' + product);
          this.lastErrorMsg = null;
          this.viewCtrl.dismiss(loader);
        },
        err => {
          console.error('Error when adding product: ' + err.error);
          console.error(err);
          this.lastErrorMsg = err.error;
          loader.dismiss();
        }
      );
  }
}
