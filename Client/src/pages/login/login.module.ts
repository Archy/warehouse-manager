import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginPage } from './login';

@NgModule({
  declarations: [
    LoginPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginPage),
  
    // AngularFireModule.initializeApp(firebaseConfig),
    // AngularFirestoreModule
  ],
  exports: [
    LoginPage,
    // AngularFireModule
  ],
})
export class LoginPageModule {}
