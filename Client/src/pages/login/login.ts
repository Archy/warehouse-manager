import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage implements OnInit {

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,  
    public authProvider:AuthProvider, 
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();

    this.authProvider.trySilentLogin()
    .then(res => {
      this.navCtrl.setRoot("ProductListPage");
      loader.dismiss();
    })
    .catch(err  => {
      loader.dismiss();
    });
  }

  ngOnInit(): void {
  }

  googleLogin() {
    console.log("Logging via google");
    this.authProvider.googleLogin()
      .then(res => {
        this.navCtrl.setRoot("ProductListPage");
      })
      .catch(err => {
        console.error(err);
        const alert = this.alertCtrl.create({
          title: 'Failed to log in',
          subTitle: 'Error was: ' + err,
          buttons: ['OK']
        });
        alert.present();
      });
  }
}
