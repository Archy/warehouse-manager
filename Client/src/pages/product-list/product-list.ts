import { Product } from './../../models/product';
import { ProductsProvider } from './../../providers/products/products';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, ModalController, LoadingController, AlertController } from 'ionic-angular';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the ProductListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product-list',
  templateUrl: 'product-list.html',
})
export class ProductListPage implements OnInit {
  currentProducts: Product[];
  lastErrorMsg: string;
  links: any;

  constructor(public navCtrl: NavController, public productsProvider: ProductsProvider, 
    public modalCtrl: ModalController, public loadingCtrl: LoadingController, 
    public alertCtrl: AlertController, public authProvider:AuthProvider) {
  }

  ngOnInit(): void {
    console.log('Loading products list');

    this.lastErrorMsg = null
    this.productsProvider.getAll().subscribe(
      productsList => {
        console.warn(productsList);
        this.currentProducts = productsList._embedded.productList;
        this.links = productsList._links;
      },
      err => {
        this.lastErrorMsg = err.error;
        console.error(err.error);
      }
    );
  }

  signOut(): void {
    this.authProvider.signOut()
    .then(res => {
      this.navCtrl.setRoot("LoginPage");
      this.navCtrl.popToRoot();
    })
    .catch(err => {
      console.error(err);
      const alert = this.alertCtrl.create({
        title: 'Failed to log out',
        subTitle: 'Error was: ' + err,
        buttons: ['OK']
      });
      alert.present();
    });
  }

  doRefresh(refresher) {
    this.lastErrorMsg = null;
    this.productsProvider.getAll().subscribe(
      productsList => {
        this.currentProducts = productsList._embedded.productList;
        this.links = productsList._links;
        refresher.complete();
      },
      err => {
        this.lastErrorMsg = err.error;
        console.error(err.error);
      }
    );
  }

  addProduct() {
    console.log('Going to add product page');
    const addModal = this.modalCtrl.create('ProductAddPage', {addLink: this.links.create.href});
    addModal.onDidDismiss(loader => {
      console.log('Comming from product add');
      if (loader) {
        this.lastErrorMsg = null
        this.productsProvider.getAll().subscribe(
          productsList => {
            this.currentProducts = productsList._embedded.productList;
            this.links = productsList._links;
            loader.dismiss();
          },
          err => {
            this.lastErrorMsg = err.error;
            console.error(err.error);
          }
        );
      }
    })
    addModal.present();
  }

  deleteProduct(product: Product) {
    console.log('Deletign product: ' + product.name);
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();

    this.productsProvider.delete(product.name, product._links.delete.href).subscribe(
      ok => {
        this.productsProvider.getAll().subscribe(productsList => {
          this.currentProducts = productsList._embedded.productList;
          this.links = productsList._links;
          loader.dismiss();
        });
      },
      err => {
        loader.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Failed to remove ' + product.name,
          subTitle: 'Error was: ' + err.error,
          buttons: ['Dismiss']
        });
        alert.present();
      },
    );
  }

  showProduct(product: Product) {
    console.log('Showing product: ' + product.name);
    this.navCtrl.push('ProductDetailsPage', {
      name: product.name,
      links: product._links,
    });
  }

}
