import { Product } from './../../models/product';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {AuthProvider} from '../auth/auth';
import { ProductList } from '../../models/productList';

const LOCAL_URL: string = 'http://localhost:8080/rest/api';
const LOCAL_IP: string = "http://192.168.1.167:8080/rest/api";
const AWS_URL: string = 'http://ec2-3-121-100-218.eu-central-1.compute.amazonaws.com:8080/rest/api'
const BASE_URL = AWS_URL;

@Injectable()
export class ProductsProvider {

  constructor(public http: HttpClient, public authProvider: AuthProvider) {
  }

  getAll(url?: string): Observable<ProductList> {
    if (url) {
      console.warn(url);
      return this.http.get<ProductList>(url, this.getOptions());
    } else {
      const u = BASE_URL + '/product'
      return this.http.get<ProductList>(u, this.getOptions());
    }
  }

  get(name: String, url: string): Observable<Product> {
    console.warn(name);
    console.warn(url);
    return this.http.get<Product>(url, this.getOptions());
  }

  addNew(name: string, manufacturer: string, price: string, url: string): Observable<Product> {
    console.warn(url);
    const body = {
      name,
      manufacturer,
      price
    };
    return this.http.post<Product>(url, body, this.getOptions());
  }

  delete(name: string, url: string): Observable<Object> {
    console.warn(url);
    return this.http.delete(url, this.getOptions());
  }

  updateQuantity(name: string, delta: number, url: string):  Observable<Product> {
    console.warn(url);
    return this.http.put<Product>(url, {delta}, this.getOptions());
  }


  private getOptions() {
    const headers = new HttpHeaders({
      'Accept':  'application/json',
      'Authorization': 'Bearer: ' + this.authProvider.idToken,
    });
    return {
      headers
    };
  }

}
