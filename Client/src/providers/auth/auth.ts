import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GooglePlus } from '@ionic-native/google-plus';
import {Platform} from "ionic-angular";
import { Storage } from '@ionic/storage';
import { JwtHelperService } from '@auth0/angular-jwt';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {
  public idToken: any;

  constructor(public http: HttpClient, private googlePlus: GooglePlus, public platform: Platform, private storage: Storage) {
    console.log('Hello AuthProvider Provider');
  }

  signOut() {
    const googleLogout = (resolve, reject) => {
      this.googlePlus.logout()
      .then(res => {
        console.info('SUCCESS')
        console.info(res)
        resolve(res);
      }, err => {
        console.error("FAILL")
        console.error(err);
        resolve(err);
      })
    };

    return new Promise((resolve, reject) => {
      this.storage.remove('idToken')
      .then(res => {
        googleLogout(resolve, reject)
      })
      .catch(err => {
        googleLogout(resolve, reject)
      })
    }); 
  }

  trySilentLogin(): Promise<any> {

    const tryStoredToken = (resolve, reject) => {
      this.storage.get('idToken')
      .then(res => {
        const helper = new JwtHelperService();
        if (res == null || helper.isTokenExpired(res)) {
          console.error("STORED TOKEN FAILL");
          reject(res);
        } else {
          console.info('STORED TOKEN SUCCESS')
          this.idToken = res;
          console.warn(this.idToken);
          resolve(res);
        }
      }, err => {
        console.error("STORED TOKEN FAILL")
        reject(err);
      });
    }
  
    return new Promise((resolve, reject) => {
      this.googlePlus.trySilentLogin({
        'webClientId': '965236565425-u22tf46t8gtb952mcqqr60f41nl7lpuh.apps.googleusercontent.com'
      })
      .then(res => {
        console.info('AUTOLOGIN SUCCESS');
        console.warn(res);
        this.idToken = res.idToken;
        this.storage.set('idToken', this.idToken);
        resolve(res);
      }, err => {
        console.error("AUTOLOGIN FAILL")
        tryStoredToken(resolve, reject);
      });
    });
  }

  private 

  googleLogin(): Promise<any> {

    return new Promise((resolve, reject) => {
      this.googlePlus.login({
        'webClientId': '965236565425-u22tf46t8gtb952mcqqr60f41nl7lpuh.apps.googleusercontent.com',
      }).then(res => {
        console.info('SUCCESS')
        console.info(res)
        this.idToken = res.idToken;
        this.storage.set('idToken', this.idToken);
        console.warn(this.idToken);
        resolve(res);
      }, err => {
        console.error("FAILL")
        console.error(err);
        reject(err);
      });
    });
  }
}
