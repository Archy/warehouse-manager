FROM openjdk:8u111-jdk-alpine

ADD /server/target/warehouse-manager-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]