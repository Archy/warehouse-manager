package com.ium.warehousemanager.rest;

import com.ium.warehousemanager.model.Product;
import com.ium.warehousemanager.service.ProductsService;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static com.ium.warehousemanager.validation.ValidationUtils.validate;
import static com.ium.warehousemanager.validation.ValidationUtils.validateExist;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/product")
public class WarehouseController {
    private static final Logger log = LogManager.getLogger(WarehouseController.class);

    @Inject
    private final ProductsService productsService;
    @Inject
    private final ProductResourceAssembler productResourceAssembler;


    public WarehouseController(final ProductsService productsService,
                               final ProductResourceAssembler productResourceAssembler) {
        this.productsService = productsService;
        this.productResourceAssembler = productResourceAssembler;
    }

    @GetMapping
    @Secured("ROLE_WORKER")
    public Resources<Resource<Product>> getAllProducts() {
        List<Resource<Product>> products = productsService.findAll().stream()
                .map(productResourceAssembler::toResource)
                .collect(Collectors.toList());

        return new Resources<>(products,
                linkTo(methodOn(WarehouseController.class).getAllProducts()).withSelfRel(),
                linkTo(methodOn(WarehouseController.class).createProduct(null, null)).withRel("create")
        );
    }

    @GetMapping("/{name}")
    @Secured("ROLE_WORKER")
    public Resource<Product> getProduct(@PathVariable("name") String name) {
        Product p = productsService.findByName(name);
        validateExist(() -> p, String.format("Product '%s' doesn't exists", name));

        return productResourceAssembler.toResource(p);
    }

    @PostMapping(consumes = "application/json")
    @Secured("ROLE_WORKER")
    public ResponseEntity createProduct(@RequestBody PostProduct newProducts, UriComponentsBuilder uriComponentsBuilder) {
        validate(() -> newProducts.name.trim().equals(newProducts.name), "Invalid product name");
        validate(() -> NumberUtils.isNumber(newProducts.price),
                String.format("Price must be a number, was: '%s'", newProducts.price));
        validate(() -> {
            BigDecimal p = new BigDecimal(newProducts.price);
            return p.signum() >= 0;
        }, "Price cannot be negative.");
        validate(() -> productsService.findByName(newProducts.name) == null,
                String.format("Product '%s' already exists", newProducts.name));

        productsService.createProduct(newProducts.name, newProducts.manufacturer, new BigDecimal(newProducts.price));

        UriComponents uriComponents = uriComponentsBuilder.path("/product/{name}").buildAndExpand(newProducts.name);
        return ResponseEntity.created(uriComponents.toUri()).build();
    }

    @DeleteMapping("/{name}")
    @Secured("ROLE_MANAGER")
    public ResponseEntity deleteProduct(@PathVariable("name") String name) {
        validateExist(() -> productsService.findByName(name),
                String.format("Product %s doesn't exists", name));
        productsService.deleteByName(name);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{name}")
    @Secured("ROLE_WORKER")
    public Resource<Product> updateQuantity(@PathVariable("name")String name, @RequestBody Delta delta) {
        log.debug("Updating quantity for " + name + " by " + delta.delta);
        validateExist(() -> productsService.findByName(name),
                String.format("Product '%s' doesn't exists", name));
        productsService.updateQuantity(name, delta.delta);

        final Product p = productsService.findByName(name);
        return productResourceAssembler.toResource(p);
    }

    private static class PostProduct {
        public String name;
        public String manufacturer;
        public String price;

        public PostProduct() {
        }

        public PostProduct(String name, String manufacturer, String price) {
            this.name = name;
            this.manufacturer = manufacturer;
            this.price = price;
        }
    }

    private static class Delta {
        public Long delta;

        public Delta() {
        }

        public Delta(Long delta) {
            this.delta = delta;
        }
    }
}
