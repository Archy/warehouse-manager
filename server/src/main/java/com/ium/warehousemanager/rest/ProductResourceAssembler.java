package com.ium.warehousemanager.rest;

import com.ium.warehousemanager.model.Product;
import com.ium.warehousemanager.service.UserService;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class ProductResourceAssembler implements ResourceAssembler<Product, Resource<Product>> {
    @Override
    public Resource<Product> toResource(Product product) {
        final List<Link> links = new ArrayList<>(Arrays.asList(
                linkTo(methodOn(WarehouseController.class).getProduct(product.getName())).withSelfRel(),
                linkTo(methodOn(WarehouseController.class).updateQuantity(product.getName(), null)).withRel("updateQuantity"),
                linkTo(methodOn(WarehouseController.class).getAllProducts()).withRel("products")
        ));

        boolean isManager = SecurityContextHolder.getContext()
                .getAuthentication()
                .getAuthorities()
                .stream()
                .anyMatch(a -> a.getAuthority().equalsIgnoreCase(UserService.ROLE_MANAGER));
        if (isManager) {
            links.add(linkTo(methodOn(WarehouseController.class).deleteProduct(product.getName())).withRel("delete"));
        }
        return new Resource<>(product, links);
    }
}
