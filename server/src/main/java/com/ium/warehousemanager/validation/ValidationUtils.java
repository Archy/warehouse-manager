package com.ium.warehousemanager.validation;

import java.util.function.Supplier;

public class ValidationUtils {
    private ValidationUtils() {
    }

    public static void validate(Supplier<Boolean> validator, String errorMsg) {
        if (!validator.get()) {
            throw new WebValidationException(errorMsg);
        }
    }

    public static void validateExist(Supplier<?> supplier, String errorMsg) {
        if (supplier.get() == null) {
            throw new ResourceMissingException(errorMsg);
        }
    }
}
