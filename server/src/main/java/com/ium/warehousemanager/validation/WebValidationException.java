package com.ium.warehousemanager.validation;

public class WebValidationException extends RuntimeException {
    public WebValidationException(String message) {
        super(message);
    }

    public WebValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}
