package com.ium.warehousemanager.validation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger log = LogManager.getLogger(RestResponseEntityExceptionHandler.class);


    @ExceptionHandler(value = { WebValidationException.class })
    protected ResponseEntity<Object> handleConflict(WebValidationException ex, WebRequest request) {
        String responseBody = ex.getMessage();
        log.error("Web validation exception: " + responseBody);
        return handleExceptionInternal(ex, responseBody, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value = { ResourceMissingException.class })
    protected ResponseEntity<Object> handleConflict(ResourceMissingException ex, WebRequest request) {
        String responseBody = ex.getMessage();
        log.error("Resource missing exception: " + responseBody);
        return handleExceptionInternal(ex, responseBody, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }
}
