package com.ium.warehousemanager.validation;

public class ResourceMissingException extends RuntimeException {
    public ResourceMissingException(String message) {
        super(message);
    }

    public ResourceMissingException(String message, Throwable cause) {
        super(message, cause);
    }
}
