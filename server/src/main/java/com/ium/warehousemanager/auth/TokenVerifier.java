package com.ium.warehousemanager.auth;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

@Component
public class TokenVerifier {
    private static final Logger log = LogManager.getLogger(TokenVerifier.class);
    private GoogleIdTokenVerifier verifier;

    public TokenVerifier() {
        this.verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), new JacksonFactory())
                .setAudience(Collections.singleton("965236565425-u22tf46t8gtb952mcqqr60f41nl7lpuh.apps.googleusercontent.com"))
                .setIssuer("https://accounts.google.com")
                .setPublicCertsEncodedUrl("https://www.googleapis.com/oauth2/v1/certs")
                .build();
    }

    @Nullable
    public GoogleIdToken verifyToken(String idToken) {
        try {
            return verifier.verify(idToken);
        } catch (GeneralSecurityException | IOException | RuntimeException e) {
            e.printStackTrace();
            log.error(e);
        }
        return null;
    }
}
