package com.ium.warehousemanager.auth;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.autoconfigure.security.oauth2.resource.AuthoritiesExtractor;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Primary
public class CustomUserInfoTokenServices extends UserInfoTokenServices implements ResourceServerTokenServices {
    private static final Logger log = LogManager.getLogger(OAuthConfig.class);

    @Inject
    private TokenVerifier tokenVerifier;
    @Inject
    private AuthoritiesExtractor authoritiesExtractor;

    public CustomUserInfoTokenServices(TokenVerifier tokenVerifier, AuthoritiesExtractor authoritiesExtractor) {
        super(null, null);
        this.tokenVerifier = tokenVerifier;
    }

    @Override
    public OAuth2Authentication loadAuthentication(String idToken) throws AuthenticationException, InvalidTokenException {
        idToken = idToken.substring(2); //TODO
        GoogleIdToken token = tokenVerifier.verifyToken(idToken);
        if (token == null) {
            log.info("bad id token");
            return null;
        }
        log.info("id token ok");
        final Map<String, Object> config = new HashMap<>();
        config.put("user", token.getPayload().getEmail());
        config.put("email", token.getPayload().getEmail());

        Object principal = getPrincipal(config);
        List<GrantedAuthority> authorities = this.authoritiesExtractor.extractAuthorities(config);
        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                principal, "N/A", authorities);

        OAuth2Request request = new OAuth2Request(null, null, null, true, null,
                null, null, null, null);

        return new OAuth2Authentication(request, auth);
    }

    @Override
    public OAuth2AccessToken readAccessToken(String accessToken) {
        throw new UnsupportedOperationException("Not supported: read access token");
    }

}
