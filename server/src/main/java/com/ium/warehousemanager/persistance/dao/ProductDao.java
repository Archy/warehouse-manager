package com.ium.warehousemanager.persistance.dao;

import com.ium.warehousemanager.persistance.model.MutableProduct;
import org.springframework.lang.Nullable;

import java.util.List;

public interface ProductDao {
    void createProduct(MutableProduct product);

    List<MutableProduct> getAllProducts();

    @Nullable
    MutableProduct getByName(String name);

    void alterQuantity(String name, long delta);

    void delete(String name);
}
