package com.ium.warehousemanager.persistance.model;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

@Entity
@Table(
        name="PRODUCT",
        indexes = { @Index(name = "IDX_NAME", columnList = "NAME") }
)
@NamedQueries({
        @NamedQuery(name = "Product.findAll", query = "SELECT p FROM MutableProduct p"),
        @NamedQuery(name = "Product.findByName", query = "SELECT p FROM MutableProduct p WHERE p.name = :name"),
        @NamedQuery(name = "Product.deleteByName", query = "DELETE FROM MutableProduct p WHERE p.name = :name"),
        @NamedQuery(name = "Product.alterQuantity", query = "UPDATE MutableProduct p SET p.quantity = quantity + :delta WHERE p.name = :name")
})
public class MutableProduct {
    @Id()
    @GeneratedValue
    @Column(name = "PRODUCT_ID")
    private long id;

    @Column(name = "NAME", unique = true, nullable = false)
    private String name;

    @Column(name = "MANUFACTURER", nullable = false)
    private String manufacturer ;

    @Column(name = "PRICE", nullable = false)
    @DecimalMin("0.0")
    private BigDecimal price;

    @Column(name = "QUANTITY", nullable = false)
    @Min(0)
    private long quantity;


    public MutableProduct() {
    }

    public MutableProduct(String name, String manufacturer, @DecimalMin("0.0") BigDecimal price, @Min(0) long quantity) {
        this.name = name;
        this.manufacturer = manufacturer;
        this.price = price;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "MutableProduct{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}
