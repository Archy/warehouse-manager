package com.ium.warehousemanager.persistance.dao;

import com.ium.warehousemanager.persistance.model.MutableProduct;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.List;

@Repository
public class ProductDaoImpl implements ProductDao {
    @PersistenceContext
    private EntityManager entityManager;


    @Override
    @Transactional
    public void createProduct(MutableProduct product) {
        entityManager.persist(product);
    }

    @Override
    @Transactional(readOnly = true)
    public List<MutableProduct> getAllProducts() {
        final TypedQuery<MutableProduct> query = entityManager.createNamedQuery("Product.findAll", MutableProduct.class);
        return query.getResultList();
    }

    @Override
    @Nullable
    @Transactional(readOnly = true)
    public MutableProduct getByName(String name) {
        final TypedQuery<MutableProduct> query = entityManager.createNamedQuery("Product.findByName", MutableProduct.class);
        query.setParameter("name", name);
        return query.getResultStream().findFirst().orElse(null);
    }

    @Override
    @Transactional()
    public void alterQuantity(String name, long delta) {
        final Query query = entityManager.createNamedQuery("Product.alterQuantity");
        query.setParameter("name", name);
        query.setParameter("delta", delta);
        query.executeUpdate();
        entityManager.clear();
    }

    @Override
    @Transactional
    public void delete(String name) {
        final Query query = entityManager.createNamedQuery("Product.deleteByName");
        query.setParameter("name", name);
        query.executeUpdate();
    }
}
