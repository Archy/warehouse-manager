package com.ium.warehousemanager.persistance.dao;

import com.ium.warehousemanager.persistance.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RolesRepository  extends JpaRepository<UserRole, Long> {
    UserRole findByRole(String role);
}
