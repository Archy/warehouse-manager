package com.ium.warehousemanager.model;

import com.ium.warehousemanager.persistance.model.MutableProduct;

import java.math.BigDecimal;
import java.util.Objects;

public class Product {
    private final String name;
    private final String manufacturer;
    private final BigDecimal price;
    private final long quantity;

    //for JAXB
    private Product() {
        this(null, null, null, 0);
    }

    public Product(final String name, final String manufacturer, final BigDecimal price, final long quantity) {
        this.name = name;
        this.manufacturer = manufacturer;
        this.price = price;
        this.quantity = quantity;
    }

    public Product(final MutableProduct product) {
        this.name = product.getName();
        this.manufacturer = product.getManufacturer();
        this.price = product.getPrice();
        this.quantity = product.getQuantity();
    }

    public String getName() {
        return name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public long getQuantity() {
        return quantity;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return quantity == product.quantity &&
                Objects.equals(name, product.name) &&
                Objects.equals(manufacturer, product.manufacturer) &&
                Objects.equals(price, product.price);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, manufacturer, price, quantity);
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}
