package com.ium.warehousemanager.service;

import java.util.Set;

public interface UserService {
    String ROLE_WORKER = "ROLE_WORKER";
    String ROLE_MANAGER = "ROLE_MANAGER";

    Set<String> findUserRoles(String email);
}
