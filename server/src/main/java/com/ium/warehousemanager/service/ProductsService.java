package com.ium.warehousemanager.service;

import com.ium.warehousemanager.model.Product;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.math.BigDecimal;
import java.util.List;

public interface ProductsService {
    @NonNull
    Product createProduct(String name, String manufacturer, BigDecimal price);

    @Nullable
    Product findByName(String name);

    @NonNull
    List<Product> findAll();

    void updateQuantity(String name, long quantityChange);

    void deleteByName(String name);
}
