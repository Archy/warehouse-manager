package com.ium.warehousemanager.service;

import com.ium.warehousemanager.model.Product;
import com.ium.warehousemanager.persistance.dao.ProductDao;
import com.ium.warehousemanager.persistance.model.MutableProduct;
import com.ium.warehousemanager.rest.ProductResourceAssembler;
import com.ium.warehousemanager.validation.WebValidationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductsServiceImpl implements ProductsService {
    private static final Logger log = LogManager.getLogger(ProductsServiceImpl.class);

    @Inject
    private final ProductDao productDao;

    @PostConstruct
    public void init() {
        productDao.createProduct(new MutableProduct("product one", "man", new BigDecimal("6.66"), 0));
        productDao.createProduct(new MutableProduct("product two", "volvo", new BigDecimal("88.12"), 6));
        productDao.createProduct(new MutableProduct("product three", "mercedes", new BigDecimal("99.99"), 111));
    }

    public ProductsServiceImpl(ProductDao productDao) {
        this.productDao = productDao;
    }

    @Override
    public Product createProduct(String name, String manufacturer, BigDecimal price) {
        final MutableProduct product = new MutableProduct(name, manufacturer, price, 0);
        productDao.createProduct(product);
        log.info(String.format("New product '%s' created", product));
        return new Product(product);
    }

    @Override
    @Nullable
    public Product findByName(String name) {
        final MutableProduct product = productDao.getByName(name);
        if (product == null) {
            log.debug("Product '" + name + "' not found.");
        }

        return product != null ? new Product(product) : null;
    }

    @Override
    public List<Product> findAll() {
        return productDao.getAllProducts().stream().map(Product::new).collect(Collectors.toList());
    }

    @Override
    public void updateQuantity(String name, long quantityChange) {
        try {
            productDao.alterQuantity(name, quantityChange);
        } catch (DataIntegrityViolationException ignored) {
            final String msg = String.format("It is not possible to change the '%s' quantity to below zero.", name);
            log.error(msg);
            throw new WebValidationException(msg);
        }
    }

    @Override
    public void deleteByName(String name) {
        productDao.delete(name);
    }
}
