package com.ium.warehousemanager.service;

import com.ium.warehousemanager.persistance.dao.RolesRepository;
import com.ium.warehousemanager.persistance.dao.UserRepository;
import com.ium.warehousemanager.persistance.model.User;
import com.ium.warehousemanager.persistance.model.UserRole;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Inject
    private final UserRepository userRepository;
    @Inject
    private final RolesRepository rolesRepository;

    public UserServiceImpl(UserRepository userRepository, RolesRepository rolesRepository) {
        this.userRepository = userRepository;
        this.rolesRepository = rolesRepository;
    }

    @PostConstruct
    public void setUp() {
        final UserRole roleWorker = rolesRepository.save(new UserRole(ROLE_WORKER));
        final UserRole roleManager = rolesRepository.save(new UserRole(ROLE_MANAGER));

        final User user1 = new User("jan.majkutewicz@gmail.com");
        user1.getRoles().add(rolesRepository.findByRole(ROLE_WORKER));
        userRepository.save(user1);


        final User user2 = new User("jan.majkutewicz@spartez.com");
        user2.getRoles().add(rolesRepository.findByRole(ROLE_WORKER));
        user2.getRoles().add(rolesRepository.findByRole(ROLE_MANAGER));
        userRepository.save(user2);
    }

    @Override
    public Set<String> findUserRoles(String email) {
        User user = userRepository.findByEmail(email);
        if(user == null)
            throw new UsernameNotFoundException("User not found");

        return user.getRoles().stream()
                .map(UserRole::getRole)
                .collect(Collectors.toSet());
    }
}
