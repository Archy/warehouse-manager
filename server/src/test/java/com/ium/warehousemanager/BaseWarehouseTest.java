package com.ium.warehousemanager;

import com.ium.warehousemanager.service.ProductsService;
import org.junit.After;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class BaseWarehouseTest {
    @Inject
    protected ProductsService productsService;

    @After
    public void cleanUp() {
        productsService.findAll().forEach(p -> productsService.deleteByName(p.getName()));
    }
}
