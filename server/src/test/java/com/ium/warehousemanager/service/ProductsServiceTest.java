package com.ium.warehousemanager.service;

import com.ium.warehousemanager.BaseWarehouseTest;
import com.ium.warehousemanager.model.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;


public class ProductsServiceTest extends BaseWarehouseTest {

    @Test
    public void testProductService() {
        assertThat(productsService.findAll(), is(empty()));
        productsService.createProduct("prod_1", "man_1", new BigDecimal("100.01"));
        productsService.createProduct("prod_2", "man_2", new BigDecimal("6.66"));
        productsService.createProduct("Aaa", "Bbb", new BigDecimal("1.23"));

        assertThat(productsService.findAll(), hasSize(3));
        assertThat(productsService.findByName("prod_1"), is(new Product("prod_1", "man_1", new BigDecimal("100.01"), 0)));
        assertThat(productsService.findByName("Aaa"), is(new Product("Aaa", "Bbb", new BigDecimal("1.23"), 0)));

        updateQunatity("prod_1", 100, 100);
        updateQunatity("prod_1", -10, 90);
        updateQunatity("prod_1", -45, 45);
        updateQunatity("prod_1", 6, 51);

        updateQunatity("prod_2", 1, 1);
        updateQunatity("prod_2", 2, 3);
        updateQunatity("prod_2", -3, 0);
        updateQunatity("prod_2", 6, 6);

        boolean exceptionThrown = false;
        try {
            updateQunatity("prod_2", -7, -1);
        } catch (Exception e) {
            System.out.println(e);
            exceptionThrown = true;
        }
        assertThat(productsService.findByName("prod_2").getQuantity(), is(6L));
        assertThat(exceptionThrown, is(true));


        productsService.deleteByName("prod_1");
        assertThat(productsService.findAll(), hasSize(2));
    }

    private void updateQunatity(String name, long delta, long expected) {
        productsService.updateQuantity(name, delta);
        assertThat(productsService.findByName(name).getQuantity(), is(expected));
    }
}
