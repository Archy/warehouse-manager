package com.ium.warehousemanager.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ium.warehousemanager.BaseWarehouseTest;
import com.ium.warehousemanager.model.Product;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;

import java.io.IOException;
import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;


public class WarehouseControllerTest extends BaseWarehouseTest {
    private HttpHelper http = new HttpHelper();

    @Before
    public void setUp() {
        assertThat(productsService.findAll(), hasSize(0));
    }

    @Test
    public void testWarehouseEndpoint() throws IOException {
        Product p1 = new Product("prod_1", "man_1", new BigDecimal("100.01"), 0);
        Product p2 = new Product("prod_2", "man_2", new BigDecimal("6.66"), 0);
        http.createProduct(p1);
        http.createProduct(p2);

        assertJsonEqualsObject(http.getAllProductsJson(), Product[].class, new Product[]{p1, p2});
        assertJsonEqualsObject(http.getProductJson(p1.getName()), Product.class, p1);
        assertJsonEqualsObject(http.getProductJson(p2.getName()), Product.class, p2);

        http.updateQuantity(p1.getName(), 10);
        http.updateQuantity(p2.getName(), 100);
        http.updateQuantity(p1.getName(), -5);
        http.updateQuantity(p2.getName(), -20);
        http.updateQuantity(p1.getName(), 1);
        http.updateQuantity(p2.getName(), 1);

        Product p1Mod = new Product(p1.getName(), p1.getManufacturer(), p1.getPrice(), 6);
        Product p2Mod = new Product(p2.getName(), p2.getManufacturer(), p2.getPrice(), 81);
        assertJsonEqualsObject(http.getAllProductsJson(), Product[].class, new Product[]{p1Mod, p2Mod});

        http.deleteProduct(p1.getName());
        assertJsonEqualsObject(http.getAllProductsJson(), Product[].class, new Product[]{p2Mod});
    }

    @Test
    public void testCreate() {
        Product p1 = new Product("prod_1", "man_1", new BigDecimal("100.01"), 0);
        Product p2 = new Product("prod_1", "man_1", new BigDecimal("-2.22"), 0);
        http.createProduct(p1);

        // same product
        http.createProduct(p1);
        assertThat(http.lastStatus, is(400));
        assertThat(http.lastBody, is(String.format("Product '%s' already exists", p1.getName())));

        //wrong price
        http.createProduct("prod2", "man_1", "abc");
        assertThat(http.lastStatus, is(400));
        assertThat(http.lastBody, is("Price must be a number, was: 'abc'"));

        // negative price
        http.createProduct(p2);
        assertThat(http.lastStatus, is(400));
        assertThat(http.lastBody, is("Price cannot be negative."));

    }

    @Test
    public void testFind() {
        assertThat(http.getProductJson("nonExisting"), is("Product 'nonExisting' doesn't exists"));
        assertThat(http.lastStatus, is(404));
    }

    @Test
    public void testUpdateQuantity() {
        Product p1 = new Product("prod_1", "man_1", new BigDecimal("100.01"), 0);
        http.createProduct(p1);

        // non existing product
        http.updateQuantity("nonExisting", 1);
        assertThat(http.lastStatus, is(404));
        assertThat(http.lastBody, is(String.format("Product 'nonExisting' doesn't exists", p1.getName())));

        //below 0
        http.updateQuantity("prod_1", -1);
        assertThat(http.lastStatus, is(400));
        assertThat(http.lastBody, is(String.format("It is not possible to change the '%s' quantity to below zero.", p1.getName())));

    }

    private <T> void assertJsonEqualsObject(String json, Class<? extends T> clazz, T expected) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        T object = mapper.readValue(json, clazz);
        assertThat(object, is(expected));
    }

    @Test
    public void testJsonComparision() throws IOException {
        Product p = new Product("Bamboo", "Atlassian", new BigDecimal("500.00"), 2);

        ObjectMapper mapper = new ObjectMapper();
        System.out.println(mapper.writeValueAsString(p));

        assertJsonEqualsObject("{\"name\":\"Bamboo\",\"manufacturer\":\"Atlassian\",\"price\":500.00,\"quantity\":2}", Product.class, p);
    }

    private static class HttpHelper {
        String lastBody;
        int lastStatus;

        String getAllProductsJson() {
            return sendRequest("/product", HttpMethod.GET);
        }

        String getProductJson(String name) {
            return sendRequest("/product/" + name, HttpMethod.GET);
        }

        void createProduct(Product p) {
            createProduct(p.getName(), p.getManufacturer(), p.getPrice().toString());
        }

        void createProduct(String name, String manufacturer, String price) {
            String body = new StringBuilder("{")
                    .append("\"name\":\"").append(name).append("\", ")
                    .append("\"manufacturer\":\"").append(manufacturer).append("\", ")
                    .append("\"price\":\"").append(price).append("\"")
                    .append(" }")
                    .toString();
            sendRequest("/product", HttpMethod.POST, body);
        }

        void deleteProduct(String name) {
            sendRequest("/product/" + name, HttpMethod.DELETE);
        }

        void updateQuantity(String name, long delta) {
            sendRequest("/product/" + name + "?delta=" + delta, HttpMethod.PUT);
        }

        private String sendRequest(String uri, HttpMethod method) {
            return sendRequest(uri, method, null);
        }

        private String sendRequest(String uri, HttpMethod method, String body) {
            final TestRestTemplate restTemplate = new TestRestTemplate();
            final HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            final HttpEntity<String> entity = new HttpEntity<>(body, headers);
            ResponseEntity<String> response = restTemplate.exchange(
                    createURL(uri),
                    method, entity, String.class);

            lastBody = response.getBody();
            lastStatus = response.getStatusCodeValue();
            return response.getBody();
        }

        private String createURL(String uri) {
            return "http://localhost:8080/rest/api" + uri;
        }
    }
}
